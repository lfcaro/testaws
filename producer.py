from twitter import *
import boto
import uuid
import boto.kinesis


 # AWS account credentials  
REGION = 'us-east-1'
STREAM = 'metacogtest'
AWS_ACCESS_KEY_ID='AKIAJJ36WUPTFTVTB5JA'
AWS_SECRET_ACCESS_KEY='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'


# Twitter account credentials 
AUTH = OAuth(
    consumer_key='rrEBxYlQLcFJJrupDBISREw0m',
    consumer_secret='D2mR2Wmz6EsGxR0JpQyuEngAMK2hfbPbYRgRFskHLsiprX5YzQ',
    token='14453779-wr9HRY2EVV50XUdgfvRa5svZn7R8YmOdyRTyXwdQz',
    token_secret='XwyeGHiFkdqOeFn2EHqAhjUiRZJ9j1wY0K6k9hK5ELdMk'
)


def connect_to_kinesis():
    return boto.kinesis.connect_to_region(
        REGION, 
        aws_access_key_id=AWS_ACCESS_KEY_ID,
        aws_secret_access_key=AWS_SECRET_ACCESS_KEY
    )


def send_to_kinesis(data, kinesis):
    data=data.encode('utf-8')
    kinesis.put_record(STREAM, data, 'partition')


def process_message(message, kinesis):
    if 'text' in message and message['lang'] == "en":
        send_to_kinesis(message['text'], kinesis)

def get_stream_iterator(auth, stream=None):
    domain = 'stream.twitter.com'
    if stream:
        domain = '%s.twitter.com' % stream
    return TwitterStream(auth=auth, domain=domain)

def consume_stream(auth, kinesis, stream=None):
    twitter_stream = get_stream_iterator(auth=auth, stream=stream)
    for msg in twitter_stream.statuses.sample():
        process_message(msg,kinesis)


def main():
    kinesis=connect_to_kinesis()
    while True:
        try:
            consume_stream(AUTH, kinesis)
        except:
            print "Ocurrio un error, reconectando..."


if __name__ == '__main__':
   main()
