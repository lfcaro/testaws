/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kconsumer;


import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Properties;
import java.util.UUID;
import java.util.List;
import java.util.*;



import java.net.UnknownHostException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.auth.*;

import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorFactory;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.DeleteTableRequest;
import com.amazonaws.services.dynamodbv2.model.*;




/**
 *
 * @author lfcaro
 */

public class Kconsumer {

    private static final String DEFAULT_APP_NAME = "metacogtestapp";
    private static final String DEFAULT_STREAM_NAME = "metacogtest";
    private static final String DEFAULT_KINESIS_ENDPOINT = "https://kinesis.us-east-1.amazonaws.com";
    private static final InitialPositionInStream DEFAULT_INITIAL_POSITION = InitialPositionInStream.TRIM_HORIZON;
    
    
    
    
    private static String awsAccessKey="AKIAI6ONPULMZRQI2XPQ";
    private static String awsSecretKey="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";
   

    public static Region region = Region.getRegion(Regions.US_EAST_1);
    public static AmazonDynamoDBClient client;
    public static String tableName = "tweetcount";
    public static String statTable = "tweetStats";

    
    private static String applicationName = DEFAULT_APP_NAME;
    private static String streamName = DEFAULT_STREAM_NAME;
    private static String kinesisEndpoint = DEFAULT_KINESIS_ENDPOINT;
    private static InitialPositionInStream initialPositionInStream = DEFAULT_INITIAL_POSITION;

    private static KinesisClientLibConfiguration kinesisClientLibConfiguration;
    private static final Log LOG = LogFactory.getLog(Kconsumer.class);
    
    
    public static int highestcount=0;
    public static int tweettotal=0;
    public static int wordcount=0;
    

    
    
    private Kconsumer() {
        super();
    }
    
    public static AWSCredentialsProvider generateCredentialProvider(){
        Properties props = System.getProperties();
        
        props.setProperty("aws.accessKeyId" ,awsAccessKey);
        props.setProperty("aws.secretKey" ,awsSecretKey);
        
        AWSCredentialsProvider credentialsProvider;
        return new SystemPropertiesCredentialsProvider();
        
    }

    private static void waitForTableToBeDeleted(String tableName) {
        System.out.println("Waiting for " + tableName + " while status DELETING...");

        long startTime = System.currentTimeMillis();
        long endTime = startTime + (10 * 60 * 1000);
        while (System.currentTimeMillis() < endTime) {
            try {
                DescribeTableRequest request = new DescribeTableRequest().withTableName(tableName);
                TableDescription tableDescription = client.describeTable(request).getTable();
                String tableStatus = tableDescription.getTableStatus();
                System.out.println("  - current state: " + tableStatus);
                if (tableStatus.equals(TableStatus.ACTIVE.toString())) return;
            } catch (ResourceNotFoundException e) {
                System.out.println("Table " + tableName + " is not found. It was deleted.");
                return;
            }
            try {Thread.sleep(1000 * 20);} catch (Exception e) {}
        }
        throw new RuntimeException("Table " + tableName + " was never deleted");
    }    
    
    
    private static void waitForTableToBecomeAvailable(String tableName) {
        System.out.println("Waiting for " + tableName + " to become ACTIVE...");

        long startTime = System.currentTimeMillis();
        long endTime = startTime + (10 * 60 * 1000);
        while (System.currentTimeMillis() < endTime) {
            DescribeTableRequest request = new DescribeTableRequest()
                    .withTableName(tableName);
            TableDescription tableDescription = client.describeTable(
                    request).getTable();
            String tableStatus = tableDescription.getTableStatus();
            System.out.println("  - current state: " + tableStatus);
            if (tableStatus.equals(TableStatus.ACTIVE.toString()))
                return;
            try { Thread.sleep(1000 * 20); } catch (Exception e) { }
        }
        throw new RuntimeException("Table " + tableName + " never went active");
    }
    
   private static void configureDynamo(){
        client = new AmazonDynamoDBClient(Kconsumer.generateCredentialProvider().getCredentials());
        client.setRegion(Kconsumer.region); 
        
        
        
        try{
        client.deleteTable(new DeleteTableRequest().withTableName(tableName));
        waitForTableToBeDeleted(tableName);  
        }catch (com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException e){
            
        }
        
        ProvisionedThroughput provisionedThroughput = new ProvisionedThroughput()
        .withReadCapacityUnits(10L)
        .withWriteCapacityUnits(10L);
        
        
        CreateTableRequest request = new CreateTableRequest().withTableName(tableName).withProvisionedThroughput(provisionedThroughput);
        
		ArrayList<AttributeDefinition> attributeDefinitions= new ArrayList<AttributeDefinition>();
		attributeDefinitions.add(new AttributeDefinition().withAttributeName("word").withAttributeType("S"));
		attributeDefinitions.add(new AttributeDefinition().withAttributeName("count").withAttributeType("N"));
		
                request.setAttributeDefinitions(attributeDefinitions);
        
		ArrayList<KeySchemaElement> tableKeySchema = new ArrayList<KeySchemaElement>();
		tableKeySchema.add(new KeySchemaElement().withAttributeName("word").withKeyType(KeyType.HASH));
		request.setKeySchema(tableKeySchema);
                
                ProvisionedThroughput ptIndex = new ProvisionedThroughput()
                .withReadCapacityUnits(5L).withWriteCapacityUnits(5L);                
                
                GlobalSecondaryIndex createDateIndex = new GlobalSecondaryIndex()
                    .withIndexName("count-index")
                    .withProvisionedThroughput(ptIndex)
                    .withKeySchema(
                        new KeySchemaElement()
                        .withAttributeName("count").withKeyType(KeyType.HASH))
                        .withProjection(new Projection()
                        .withProjectionType("ALL"));
          
                
                
                
                
                request=request.withGlobalSecondaryIndexes(createDateIndex);
                
                
        
        client.createTable(request);
        waitForTableToBecomeAvailable(tableName); 
        resetStats();
        
       
       
   }
   
   
    private static void resetStats(){
        resetStats("highestCount");
        resetStats("tweetTotal");
        resetStats("wordCounttotal");
    }
    
    private static void resetStats(String stat){
                    String  tableName=Kconsumer.statTable;
                    Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
                    item.put("stat", new AttributeValue(stat));        
                    item.put("value",new AttributeValue().withN("0"));                                        
                    PutItemRequest updateItemRequest = new PutItemRequest()
                    .withTableName(tableName)
                    .withItem(item);        
                    Kconsumer.client.putItem(updateItemRequest);                          
        
    }
    
    
    public static void updateStats(String stat,String value,String word,String word2){
                    
                    String  tableName=Kconsumer.statTable;
                    HashMap<String, AttributeValue> key = new HashMap<String, AttributeValue>();
                    Map<String, AttributeValueUpdate> item = new HashMap<String, AttributeValueUpdate>();
                    HashMap<String, ExpectedAttributeValue> expitem= null;
                    key.put("stat", new AttributeValue(stat));        
                    item.put("sample",new AttributeValueUpdate().withAction(AttributeAction.PUT).withValue(new AttributeValue().withS(word)));
                    if(stat.equals("wordCounttotal")){
                            item.put("sample1",new AttributeValueUpdate().withAction(AttributeAction.PUT).withValue(new AttributeValue().withS(word2)));   
                    }    
                    
                    if(stat.equals("tweetTotal")||stat.equals("wordCounttotal")){    
                        item.put("value",new AttributeValueUpdate().withAction(com.amazonaws.services.dynamodbv2.model.AttributeAction.ADD).withValue(new AttributeValue().withN("+1")));                                        
                    
                    }else if (stat.equals("highestCount")){
                        
                        expitem = new HashMap<String, ExpectedAttributeValue>();
                        item.put("value",new AttributeValueUpdate()
                                    .withAction(com.amazonaws.services.dynamodbv2.model.AttributeAction.PUT)
                                    .withValue(new AttributeValue().withN(value)));

                        expitem.put("value",new ExpectedAttributeValue()
                                    .withComparisonOperator("LT")
                                    .withAttributeValueList(new AttributeValue().withN(value)));
                        
                    }
                    
                    UpdateItemRequest updateItemRequest = new UpdateItemRequest()
                    .withTableName(tableName)
                    .withKey(key)
                    .withAttributeUpdates(item);  
                   
                    if(expitem!=null){
                        updateItemRequest.withExpected(expitem); 
                    }
                    try {
                    Kconsumer.client.updateItem(updateItemRequest);        
                    }catch (Exception e){
                        
                    }
        
    }
       

    
    private static void configure() {
        
        String workerId ;
        try{
            workerId=InetAddress.getLocalHost().getCanonicalHostName() + ":" + UUID.randomUUID();
        } catch (UnknownHostException e){
            workerId="lfcaro:" + UUID.randomUUID();
        }
        LOG.info("Using workerId: " + workerId);
        
        AWSCredentialsProvider credentialsProvider = generateCredentialProvider();
        
        
        
        kinesisClientLibConfiguration = new KinesisClientLibConfiguration(applicationName, streamName,credentialsProvider, workerId).withInitialPositionInStream(initialPositionInStream);
        
        
         
    }

    
    
    
    public static void main(String[] args) {
        System.out.println("running metacogtest");
        configure();
        configureDynamo();
        
        
        IRecordProcessorFactory recordProcessorFactory = new RecordFactory();
        
        
        Worker worker = new Worker(recordProcessorFactory, kinesisClientLibConfiguration);

        int exitCode = 0;
        try {
            worker.run();
        } catch (Throwable t) {
            LOG.error("Caught throwable while processing data.", t);
            exitCode = 1;
        }
        
        System.exit(exitCode);
        
        
        
    }
    
}
