/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kconsumer;

import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorFactory;


/**
 *
 * @author lfcaro
 */
public class RecordFactory implements IRecordProcessorFactory {
    
    public RecordFactory() {
        super();
    }

    
    @Override
    public IRecordProcessor createProcessor() {
        return new Recordprocesor();
    }

    
}
